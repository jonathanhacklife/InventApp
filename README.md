# InventApp

Un generador de Brainstorming artificial.

## Descripción

Muchos de nosotros no contamos con un equipo formalizado a la hora de crear un nuevo proyecto, por lo que hacer un brainstorming (lluvia de ideas) solitariamente nos termina llevando a imaginar ideas poco favorables e incluso nos deja inconcientes que de todos los recursos que disponemos solo nos centramos en 3 o 4 de ellos.

Es por eso que creé esta pequeña aplicación para poder abarcar la mayor cantidad de ideas con las cosas que nos rodean, de una manera más sencilla y óptima para quienes buscamos crear constantemente.

## Funcionamiento
En primera instancia esta aplicación está desarrollada en base a una lista de recursos que disponemos para luego generar una lluvia de ideas entre todos estos, permitiendonos idear resultados desde su unión.

Próximamente se dispondrá de una pantalla de creación de recursos por categoría, para que el usuario pueda hacer brainstorming de lo que sea, desde la organización empresarial, ideas para un futuro emprendimiento, y hasta una fiesta de cumpleaños! (Ideal para los que quieren crear un momento único)

### Vealo en acción aqui: https://jonathanhacklife.gitlab.io/InventApp/