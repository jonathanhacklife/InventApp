﻿// En esta lista ingresaremos los recursos disponibles

// Almacenamos el objeto de LocalStorage para su posterior uso
let storage = window.localStorage;

class ManageResource {
  constructor() {
    this.listaDeRecursos = [];
  }

  get getListaDeRecursos() {
    return this.listaDeRecursos;
  }

  set setListaDeRecursos(valor) {
    this.listaDeRecursos = valor
    this.saveLocalStorage(this.getListaDeRecursos, "milista");
  }

  //* CREATE
  addToList(elemento) {
    // Comprobamos que la entrada no sea nula, vacía y que no esté dentro de la lista de recursos
    if (elemento != null && elemento.trim() != "" && !this.getListaDeRecursos.includes(elemento)) {
      this.setListaDeRecursos = this.getListaDeRecursos.concat(elemento);
    }
  }

  //* UPDATE
  actualizarItem(valor1, valor2) {
    if (this.getListaDeRecursos.includes(valor1) && !this.getListaDeRecursos.includes(valor2)) {
      this.getListaDeRecursos[this.getListaDeRecursos.indexOf(valor1)] = valor2;
      this.setListaDeRecursos = this.getListaDeRecursos
    }
    else if (!this.getListaDeRecursos.includes(valor1))
      alert("El ítem escrito no se encuentra en la lista de recursos.");
    else if (this.getListaDeRecursos.includes(valor2))
      alert("El ítem escrito ya se encuentra en la lista de recursos.");
  }

  //* DELETE
  // Utilizo SPLICE para recortar un elemento de la lista a partir de su índice
  eliminarItem(recurso) {
    if (this.getListaDeRecursos.includes(recurso)) {
      this.getListaDeRecursos.splice(this.getListaDeRecursos.indexOf(recurso), 1)
      this.setListaDeRecursos = this.getListaDeRecursos
    }
    else
      alert("El ítem no se encuentra en la lista de recursos.")
  }

  eliminarTodo() {
    // Eliminamos el contenido de LocalStorage y la lista local
    storage.clear()
    this.setListaDeRecursos = [];
  }

  generarListaRandom() {
    // Copio la lista original para reordenarla aleatoriamente
    let randomList = [...this.getListaDeRecursos];

    // Algoritmo de Fisher-Yate para ordenar aleatoriamente una lista
    for (var i = randomList.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = randomList[i];
      randomList[i] = randomList[j];
      randomList[j] = temp;
    }
    return randomList;
  }

  saveLocalStorage(info, tag) {
    /* localStorage solo admite cadenas de texto (strings).
    Utilizo JSON.stringify() y JSON.parse() para formatearlo. */
    storage.setItem(tag, JSON.stringify(info));
    verRecursos();
  }
  loadLocalStorage(tag) {
    if (JSON.parse(storage.getItem(tag)) != null) {
      setListaDeRecursos = Array.from(JSON.parse(storage.getItem(tag)));
    }
  }
}


// PAGE FUNCTIONS
const verRecursos = () => {
  document.getElementById("listaDeRecursos").innerHTML = `
  <dt>Dispongo de estos recursos:</dt>
  <dl id="recursos"></dl>`;

  for (resource of list.getListaDeRecursos) {
    document.getElementById("recursos").innerHTML += `
      <dd>
        <div class="center-h">
          <button type="button" class="btn" name="btn-close" onclick="list.eliminarItem('${resource}')">❌</button>
          <p id=${resource}>${resource}</p>
          <button type="button" class="btn" name="btn-refresh" onclick="list.actualizarItem('${resource}', prompt('Escriba el nuevo nombre del ítem'))">🔄</button>
        </div>
      </dd>`
  }

  if (list.getListaDeRecursos.length >= 2)
    showAll();
  else if (list.getListaDeRecursos.length == 1)
    resetButtons();
  else {
    resetButtons();
    resetList();
  }
}

const generarBrainstorming = () => {
  let lista = list.generarListaRandom();
  document.getElementById("resultadoDeBrainstorming").style.display = "block";
  document.getElementById("resultadoDeBrainstorming").innerHTML = `
    <div id="resultado" class="center-h center-v">
      <h1>Podrías hacer algo mezclando ${lista[0]} con ${lista[1]}</h1>
      <div id="bottomButton">
        <button id="BTN-GenerarBrainstorming" type="button" class="btn bouncy" onclick="generarBrainstorming()" style="display: block">💡</button>
      </div>
    </div>
    `;
}

const showSnackbar = (texto) => {
  document.getElementsByTagName('footer')[0].innerHTML = `<div id="snackbar" class="show">${texto}</div>`;
  let snackbar = document.getElementById("snackbar");
  setTimeout(() => { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
}

const showAll = () => {
  document.getElementById("BTN-EliminarTodo").style.display = "block"
  document.getElementById("BTN-GenerarBrainstorming").style.display = "block"
  document.getElementById("BTN-Probar").style.display = "none"
}

const resetButtons = () => {
  document.getElementById("BTN-Probar").style.display = "block"
  document.getElementById("BTN-EliminarTodo").style.display = "none"
  document.getElementById("BTN-GenerarBrainstorming").style.display = "none"
  document.getElementById("resultadoDeBrainstorming").style.display = "none"
}

const resetList = () => {
  document.getElementById("listaDeRecursos").innerHTML = `<p>Para empezar a generar ideas seleccione el botón "+".</p>`
}

let list = new ManageResource();

document.getElementById("BTN-Agregar").addEventListener('click', () => { list.addToList(prompt('Ingrese un nuevo recurso:')) })
document.getElementById("BTN-Probar").addEventListener('click', () => { cargarTest() })
document.getElementById("BTN-EliminarTodo").addEventListener('click', () => { list.eliminarTodo() })
document.getElementById("BTN-GenerarBrainstorming").addEventListener('click', () => { generarBrainstorming() })

const cargarTest = () => {
  // FEATURE: Agregar opción de importar recursos desde un CSV
  list.addToList("Bluetooth");
  list.addToList("Giroscópio");
  list.addToList("Sensor de Proximidad");
  list.addToList("Tweets");
  list.addToList("Relays");
  list.addToList("LEDs");
  list.addToList("Sensor de sonido");
  list.addToList("QR");
}


// GENERATE
// Método uno:
/**
 * [Algoritmo de Fisher-Yate]
 *
 * El bucle debe iterar sobre la matriz, de atrás hacia adelante sin pasar por el índice 0. El bucle debe ejecutarse (array.length - 1) veces.
 * Cada paso de bucle genera un número aleatorio que varía entre 0 e i.
 * Cada paso de bucle Intercambia el valor del índice i con el valor encontrado en el índice j.
 *
 * Afectando la posición del índice i de la matriz.
 * Genere un número aleatorio entre 0 e i.
 * Generado: j.
 * Intercambiar valores encontrados en el índice i y el índice j.
 *
 * Link: [http://www.developphp.com/video/JavaScript/Fisher-Yates-Shuffle-Modern-Algorithm-Array-Programming-Tutorial]
 */